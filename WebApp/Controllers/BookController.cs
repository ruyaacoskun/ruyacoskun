﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Models;
using System.Web.Security;
using Microsoft.AspNet.Identity;

namespace WebApp.Controllers
{
    public class BookController : Controller
    {
        protected DB db = new DB();

        // GET: Book
        public ActionResult Index()
        {
            return RedirectToAction("Books", "Home");
        }
        public ActionResult Goruntule(int? id)
        {
            Kitaplar Kitap = db.Kitaplar.Where(o => o.Id == id).FirstOrDefault();

            try
            {
                if (Kitap.Id > 0)
                    ViewBag.Kitap = Kitap;
                else
                    ViewBag.NotFound = true;
            }
            catch (Exception)
            {
                ViewBag.NotFound = true;
            }

            if (Request.IsAuthenticated)
                ViewBag.IsAuthenticated = true;
            else
                ViewBag.IsAuthenticated = false;

            return View();
        }

        [HttpPost]
        public ActionResult SatinAl(int Id)
        {
            if (Request.IsAuthenticated)
            {
                var kitap = db.Kitaplar.Single(o => o.Id == Id);
                kitap.Satilan += 1;

                Siparisler yeni_siparis = new Siparisler();
                yeni_siparis.KitapId = Id;
                yeni_siparis.UserId = User.Identity.GetUserId();
                yeni_siparis.Tarih = DateTime.Now;

                db.Siparisler.Add(yeni_siparis);

                db.SaveChanges();

                return Content("1");
            }
            else
            {
                return Content("0");
            }
        }

        [HttpPost]
        [Authorize(Roles="Admin")]
        public ActionResult Delete(int Id)
        {
            try
            {
                var kitap = db.Kitaplar.Single(o => o.Id == Id);
                db.Kitaplar.Remove(kitap);
                if (db.SaveChanges() > 0)
                    return Content("1");
                else
                    return Content("0");
            }
            catch (Exception)
            {
                return Content("0");
            }
            
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult Vitrin(int Id)
        {
            try
            {
                int task = 0;

                var kitap = db.Kitaplar.Single(o => o.Id == Id);
                if (kitap.Vitrin == 0)
                    task = 1;

                kitap.Vitrin = task;
                if (db.SaveChanges() > 0)
                    return Content(task.ToString());
                else
                    return Content("2");
            }
            catch (Exception)
            {
                return Content("2");
            }

        }

        [Authorize(Roles="Admin")]
        public ActionResult Add(Kitaplar kitap)
        {
            if (Request.HttpMethod == "POST")
            {
                if (kitap.KitapAdi != "" && kitap.ResimUrl != "" && kitap.Fiyat.ToString() != "" && kitap.Aciklama != "")
                {
                    Kitaplar yeni_kitap = new Kitaplar();
                    yeni_kitap.KitapAdi = kitap.KitapAdi;
                    yeni_kitap.Aciklama = kitap.Aciklama;
                    yeni_kitap.ResimUrl = kitap.ResimUrl;
                    yeni_kitap.Fiyat = kitap.Fiyat;
                    yeni_kitap.Satilan = 0;
                    yeni_kitap.Vitrin = 0;

                    db.Kitaplar.Add(yeni_kitap);
                    if (db.SaveChanges() > 0)
                    {
                        ViewBag.MessageOK = "Kitap başarıyla eklendi.";
                    }
                    else
                    {
                        ViewBag.MessageError = "Bilinmeyen bir sorun oluştu lütfen daha sonra tekrar deneyin.";
                    }
                }
                else
                {
                    ViewBag.MessageError = "Lütfen bütün alanları doldurun!";
                }
            }
            

            return View();
        }
    }
}