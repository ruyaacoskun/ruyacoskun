﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        protected DB db = new DB();
        public ActionResult Index()
        {
            List<Kitaplar> Vitrin = db.Kitaplar.Where(o => o.Vitrin == 1).ToList();
            ViewBag.Vitrindekiler = Vitrin;

            List<Kitaplar> EnCokSatanlar = db.Kitaplar.OrderByDescending(o => o.Satilan).Take(12).ToList();
            ViewBag.EnCokSatanlar = EnCokSatanlar;

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Hakkımızda.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Iletisim";

            return View();
        }

        public ActionResult Books()
        {
            List<Kitaplar> Kitaplar = db.Kitaplar.ToList();
            ViewBag.Kitaplar = Kitaplar;

            return View();
        }

    }
}