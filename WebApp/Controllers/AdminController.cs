﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class AdminController : Controller
    {

        DB db = new DB();

        // GET: Admin
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            List<Siparisler> Siparisler = db.Siparisler.OrderByDescending(o => o.Id).ToList();
            ViewBag.Siparisler = Siparisler;

            List<Kitaplar> Kitaplar = db.Kitaplar.ToList();
            ViewBag.Kitaplar = Kitaplar;

            return View();
        }

    }
}