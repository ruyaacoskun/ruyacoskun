﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApp.Models;
using Microsoft.AspNet.Identity;
using System.Web.Security;

namespace WebApp.Models
{
    public class Yardimci
    {

        public static DB db = new DB();

        public static string KitapAdiniGetir(int id)
        {
            try
            {
                var kitap = db.Kitaplar.Where(o => o.Id == id).FirstOrDefault();
                return kitap.KitapAdi;
            }
            catch (Exception)
            {
                return "bulunamadı.";
            }
            
        }

        public static string KullaniciEmailGetir(string id)
        {
            try
            {
                var user = db.AspNetUsers.Where(o => o.Id == id).FirstOrDefault();
                return user.Email;
            }
            catch (Exception)
            {
                return "bulunamadı.";
            }
            
        }
    }
}