//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApp.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Kitaplar
    {
        public int Id { get; set; }
        public string KitapAdi { get; set; }
        public string Aciklama { get; set; }
        public string ResimUrl { get; set; }
        public Nullable<double> Fiyat { get; set; }
        public Nullable<int> Satilan { get; set; }
        public Nullable<int> Vitrin { get; set; }
    }
}
